# Set the base image to node:12-alpine
FROM node:12-alpine as build

# Specify where our app will live in the container
WORKDIR /app

# Copy the React App to the container
COPY . /app/

# Prepare the container for building React
RUN yarn install
RUN yarn global add react-scripts@3.0.1
# We want the production version
#ARG KEYCLOAK_URL
#ENV REACT_APP_KEYCLOAK_URL=$KEYCLOAK_URL
#ARG KEYCLOAK_REALM
#ENV REACT_APP_KEYCLOAK_REALM=$KEYCLOAK_REALM
#ARG KEYCLOAK_CLIENT_ID
#ENV REACT_APP_KEYCLOAK_CLIENT_ID=$KEYCLOAK_CLIENT_ID

RUN yarn run build

# Prepare nginx
FROM nginx:1.16.0-alpine
COPY --from=build /app/build /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/conf.d

# Fire up nginx
EXPOSE 3000
CMD ["nginx", "-g", "daemon off;"]
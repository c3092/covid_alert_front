import Keycloak from 'keycloak-js';

const keycloak = new Keycloak({
    url: "https://covid-alert-polytech-keycloak.cluster-ig5.igpolytech.fr/auth/",
    realm: "covidAlertRealm",
    clientId: "CovidAlertClientReactProd",
});

export default keycloak;
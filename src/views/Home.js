import * as React from 'react'
import { useKeycloak } from '@react-keycloak/web'
import Button from "@mui/material/Button";
import Stack from "@mui/material/Stack";
import {useHistory} from "react-router-dom";
import AxiosInstance from "../utils/AxiosInstance";
import {Alert, Grid, Typography} from "@mui/material";
import {useEffect, useState} from "react";
import {createStyles, makeStyles} from "@mui/styles";

//import { useAxios } from '../utils/hooks'

const Home  = () => {

    const { keycloak } = useKeycloak()
    const history = useHistory();
    const [successOrError,setSuccessOrError] = useState(null)
    const [disabled,setDisabled] = useState(false)
    const [kafkaUp,setKafkaUp] = useState(false);
    const classes = useStyles()
    const postPositive = async () => {
        if(keycloak?.authenticated){
            keycloak.loadUserProfile().then((data)=>{
                let userId = {userId: data.id}
                setDisabled(true);
                AxiosInstance.post("localisation/positive",userId).then(()=>{
                    setSuccessOrError(true);
                }).catch(()=>{
                    setSuccessOrError(false);
                }).finally(()=>setDisabled(false))
            });
        }
    }

    useEffect( () => {
        AxiosInstance.get(`localisation_kafka/ping`)
            .then(response => {
                setKafkaUp(response.data)
            })
    },[])
    return (
        <div>

            {!!keycloak?.authenticated && (
                <div>
                    <Stack spacing={2} direction="row">
                        <Button variant="contained" onClick={() => { history.push("/account")}}>
                            Mon compte
                        </Button>
                        <Button variant="contained" onClick={() => { history.push("/localisation")}}>
                            Mes localisations
                        </Button>
                        <Button variant="contained" onClick={() => { history.push("/document")}}>
                            Mes documents
                        </Button>
                        <Button variant="contained" onClick={() => { postPositive()}} disabled={!kafkaUp || disabled}>
                            Se declarer positif
                        </Button>
                        {successOrError === true &&
                        <Alert onClose={() => {setSuccessOrError(null)}}>Déclaration réussie</Alert>
                        }
                        {successOrError === false &&
                        <Alert onClose={() => {setSuccessOrError(null)}} severity="error">Déclaration impossible</Alert>
                        }
                    </Stack>
                </div>
            )}
        </div>
    )
}

const useStyles = makeStyles((theme) =>
    createStyles({

        title:{
            color: "white",

        },

    }),
);


export default Home
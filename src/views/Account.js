import * as React from 'react'
import { useKeycloak } from '@react-keycloak/web'
import {useHistory} from "react-router-dom";
import {Box, Button, CircularProgress, Container, TextField} from "@mui/material";
import {useEffect, useState} from "react";
import {Stack} from "@mui/material";
import AxiosInstance from "../utils/AxiosInstance";

const Account  = () => {

    const {keycloak} = useKeycloak()
    const history = useHistory();

    const [disabled,setDisabled] = useState(true);

    const [errorProfile,setErrorProfile] = useState(false);

    const [profile,setProfile] = useState(null)

    const [disabledCredentials, setDisabledCredentials] = useState(true)

    const [errorCredentials,setErrorCredentials] = useState(false)

    useEffect(()=>{
        if(keycloak?.authenticated){
            keycloak.loadUserProfile().then((data)=> {
                setProfile({
                    id: data.id,
                    firstname: data.firstName,
                    lastname: data.lastName,
                    newPassword: null,
                    confirmPassword: null,
                })
                document.getElementById("firstname").value = data.firstName
                document.getElementById("lastname").value = data.lastName
            })
        }
    },[errorProfile])

    const handleChange = (parameter,value) => {
        setProfile(oldProfile => {
            oldProfile[parameter] = value;
            return oldProfile;
        })
    }



    const updateProfile = () => {
        setDisabled(true)
        setErrorProfile(false)
        AxiosInstance.put("users/" + profile.id,
            {
                firstname: profile.firstname,
                lastname: profile.lastname,
                newPassword: null,
                confirmPassword: null
            })
            .then((data) => {
            }).catch(()=>{
                setProfile(oldProfile => {
                    oldProfile["firstname"] = profile.firstname;
                    oldProfile["lastname"] = profile.lastname;
                    return oldProfile;
                })
                setErrorProfile(true)
            })
    }

    const postCredentials = () => {
        setDisabledCredentials(!disabledCredentials)
        setErrorCredentials(false)
        AxiosInstance.put("users/"+ profile.id,
            {
                firstname: null,
                lastname: null,
                newPassword: profile.newPassword,
                confirmPassword: profile.confirmPassword
            })
            .then((data) => {
                if (data.data === 1) {
                    setErrorCredentials(true)
                }
                if(data.data === 0) {

                }
            })
    }



    return (
        <Container>
            <div>
                <Button variant="contained" onClick={()=>{
                    history.push("/home")
                }}>
                    Back
                </Button>
            </div>
            <Box sx={{
                display : "flex",
                justifyContent : "center"
            }} m="auto">
            { profile === null  &&
                    <CircularProgress />
            }
            { profile !== null  &&
                <Stack>
                    <div>
                        <h1 style={{ color: 'white' }}> Mon Profil </h1>
                    </div>

                    <Stack spacing={3} direction="row">
                        <TextField
                            error={errorProfile}
                            disabled={disabled}
                            id="firstname"
                            label="Firstname"
                            defaultValue={profile.firstname}
                            onChange={
                                (event) => {
                                    handleChange("firstname",event.target.value)
                                }
                            }
                            helperText={errorProfile?"Error Occured":""}
                            type="text"

                            InputLabelProps={{
                                shrink: true,
                                style: { color: 'white' }
                            }}
                            variant={"standard"}
                            InputProps={{
                                style: { backgroundColor: 'white' },
                            }}
                        />
                        <TextField
                            error={errorProfile}
                            disabled={disabled}
                            id="lastname"
                            label="Lastname"
                            defaultValue={profile.lastname}
                            onChange={
                                (event) => {
                                    handleChange("lastname",event.target.value)
                                }
                            }
                            helperText={errorProfile?"Error Occured":""}
                            type="text"
                            InputLabelProps={{
                                shrink: true,
                                style: { color: 'white' }
                            }}
                            variant={"standard"}
                            InputProps={{
                                style: { backgroundColor: 'white' },
                            }}
                        />
                    </Stack>

                    <div>
                        {disabled === true &&
                        <Button variant="contained" onClick={()=>{
                            setDisabled(!disabled)
                        }}>
                            Update
                        </Button>
                        }
                        {disabled === false &&
                        <Button variant="contained" onClick={updateProfile}>
                            Confirm
                        </Button>

                        }
                    </div>

                    <Stack spacing={3}>
                        <h2 style={{ color: 'white' }}>Mon mot de passe</h2>
                        <Stack spacing={3} direction="row">
                            <TextField
                                error={errorCredentials}
                                disabled={disabledCredentials}
                                id="newPassword"
                                label="New Password"
                                onChange={
                                    (event) => {
                                        handleChange("newPassword",event.target.value)
                                    }
                                }
                                helperText={errorCredentials?"Passwords are not matching":""}
                                type="password"
                                InputLabelProps={{
                                    shrink: true,
                                    style: { color: 'white' }
                                }}
                                variant={"standard"}
                                InputProps={{
                                    style: { backgroundColor: 'white' },
                                }}
                            />
                            <TextField
                                error={errorCredentials}
                                disabled={disabledCredentials}
                                id="confirmPassword"
                                label="Confirm Password"
                                onChange={
                                    (event) => {
                                        handleChange("confirmPassword",event.target.value)
                                    }
                                }
                                type="password"
                                helperText={errorCredentials?"Passwords are not matching":""}
                                InputLabelProps={{
                                    shrink: true,
                                    style: { color: 'white' }
                                }}
                                variant={"standard"}
                                InputProps={{
                                    style: { backgroundColor: 'white' },
                                }}
                            />
                        </Stack>
                        <div>
                            {disabledCredentials === true &&
                            <Button variant="contained" onClick={()=>{
                                setDisabledCredentials(!disabledCredentials)
                            }}>
                                Change Password
                            </Button>
                            }
                            {disabledCredentials === false &&
                            <Button variant="contained" onClick={postCredentials}>
                                Confirm
                            </Button>

                            }
                        </div>

                    </Stack>

                </Stack>
            }
            </Box>
        </Container>


    )
}

export default Account;
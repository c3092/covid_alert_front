import * as React from 'react'
import { useCallback } from 'react'
import { Redirect, useLocation } from 'react-router-dom'

import { useKeycloak } from '@react-keycloak/web'


const Register = () => {

    const {keycloak} = useKeycloak()
    return (
        <>
            {!!keycloak?.authenticated ? (
                <Redirect to={"/home"}/>
            ):keycloak.register()}
        </>

    )
}

export default Register
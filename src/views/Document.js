import * as React from 'react'
import {useEffect, useState} from "react";
import {useKeycloak} from "@react-keycloak/web";
import axios from "axios";

import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Modal from '@mui/material/Modal';
import {InputLabel, MenuItem, Select} from "@mui/material";
import {DataGrid} from "@mui/x-data-grid";
import AxiosInstance from "../utils/AxiosInstance";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: "#282c34",
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

const Document = () => {

    const [userId, setUserId] = useState("")
    const [documents, setDocuments] = useState([])

    const [open, setOpen] = React.useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    const [file, setFile] = useState(null);
    const [type, setType] = useState("");

    const cols = [
        { field: 'type', headerName: 'Document type', width: 300 },
        { field: 'creation_date', headerName: 'Date', width: 300 },
        { field: 'document_url', headerName: 'Document',width: 200 },
        { field:'delete', headerName: 'Delete', width: 200}
    ]

    const {keycloak} = useKeycloak()

    const [headers,setHeaders] = useState(null);

    const handleSubmit = () =>{
        var form = document.forms.namedItem("sendFile");
        let formData = new FormData(form);

        formData.append('date', Date.now())
        formData.append('user', userId)

        AxiosInstance.post("documents",formData)
            .then(() => {
                getDocuments()
            })

        setOpen(false);

    }

    const handleChange = (event) => {
        if(event.target.name == 'type'){
            setType(event.target.value)
        } else {
            setFile(event.target.value)
        }

    };


    const handleCellClick = (event) =>{
        if(event.field == "document_url"){
            AxiosInstance.get("documents/download/"+event.id, {responseType: 'arraybuffer'})
                .then((response) => {
                    console.log(response)
                    var file = new Blob([response.data], { type: 'application/pdf' });
                    var fileURL = URL.createObjectURL(file);
                    window.open(fileURL);
                })
        }
        else if (event.field == "delete"){
            AxiosInstance.delete("documents/"+event.id)
                .then(() =>  {
                    getDocuments()
                })
        }
    }

    useEffect(()=>{
        if(keycloak?.authenticated){
            keycloak.loadUserProfile().then((data)=>{
                setUserId(data.id)

                    AxiosInstance.get("documents")
                    .then((response) => {
                        let data = response.data
                        setDocuments(data)
                    })
            })
        }


    }, [])

    const getDocuments = () => {

        AxiosInstance.get("documents")
            .then((response) => {
                let data = response.data
                setDocuments(data)
            })
    }

    return (
        <div>
            <Button variant="contained" onClick={handleOpen} sx={{ m: 0.5 }}>Ajouter un document</Button>
            <br/>
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <h2>Ajouter un document</h2>
                    <form encType="multipart/form-data" onSubmit={handleSubmit} name="sendFile">
                        <br/>
                        <InputLabel id="demo-simple-select-label">Type</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={type}
                            label="Age"
                            name="type"
                            onChange={handleChange}
                        >
                            <MenuItem value={"Covid test"}>Covid test</MenuItem>
                            <MenuItem value={"Vaccination certificate"}>Vaccination certificate</MenuItem>
                        </Select>
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                        <input type="file" name="document" onChange={handleChange}/>
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                        <Button variant="contained" onClick={handleSubmit}>Ajouter un document</Button>
                        <br/>
                        <br/>
                    </form>

                </Box>
            </Modal>

            <div style={{ height: 400, width: '100%' }}>
                <DataGrid
                    rows={documents}
                    getRowId={(r) => r.file_id}
                    columns={cols}
                    pageSize={5}
                    rowsPerPageOptions={[5]}
                    onCellClick={handleCellClick}
                />
            </div>
        </div>
    )
}

export default Document
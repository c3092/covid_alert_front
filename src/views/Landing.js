import * as React from 'react'
import { useEffect, useState} from 'react'
import {useHistory} from "react-router-dom";
import { useKeycloak } from '@react-keycloak/web'
import { createStyles, makeStyles } from '@mui/styles';
import {Box, Container, Grid} from "@mui/material";
import AxiosInstance from "../utils/AxiosInstance";
import Typography from "@mui/material/Typography";
import newsLogo from "../assets/news.png";
import dataLogo from "../assets/data.png";
import CustomCarousel from "../components/CustomCarousel";
import InfoBox from "../components/InfoBox";
import LineChart from "../components/LineChart";
import Widget from "../components/Widget";


const Landing  = () => {

    const { keycloak } = useKeycloak()
    const classes = useStyles();
    const history = useHistory();
    const [news,setNews] = useState([])
    const [data,setData] = useState([])

    const routeChange = (path) =>{
        history.push(path);
    }

    const formatter = new Intl.NumberFormat('fr-FR', {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2,
    });
    const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric',hour:'2-digit',minute:'2-digit' };





    useEffect( () => {
        AxiosInstance.get(`news`)
            .then(response => {
                let newsFromAPI =response.data.slice(0,9)
                newsFromAPI.forEach(function (n,index) {
                    n.key = index;
                    n.date = new Date(Date.parse(newsFromAPI[index].date)).toLocaleDateString("fr-FR",options)
                });
                newsFromAPI.sort(function (a, b) {
                    let dateA = new Date(a.date), dateB = new Date(b.date)
                    return dateB - dateA
                });
                setNews(newsFromAPI)

            })
            .catch(()=>{});

        AxiosInstance.get(`news/data`)
            .then(response => {
                let dataFromAPI = response.data
                console.log(dataFromAPI)
                setData(dataFromAPI)
                console.log(dataFromAPI[dataFromAPI.length-1])
                console.log(dataFromAPI[dataFromAPI.length-8])
            }).catch(()=>{});

    },[])

    return (

        <div>
            <Container maxWidth="xl">
                <Typography gutterBottom={true} variant="h2" component="h2" className={classes.title}>
                    Actualités
                    <img src={newsLogo} alt="logo" className={classes.logo} />
                </Typography>
                {news.length > 0 &&  <CustomCarousel data={news}/>}
                <div>

                    {data.length > 8 &&
                    <>
                        <Box mt={5}>
                            <Typography gutterBottom={true} variant="h2" component="h2" className={classes.title}>
                                Données du { new Date(Date.parse(data[data.length-1].date)).toLocaleDateString("fr-FR",{ weekday: 'long', year: 'numeric', month: 'long', day: 'numeric'})}
                                <img src={dataLogo} alt="logo" className={classes.logo} />
                            </Typography>
                        </Box>
                        <Grid container spacing={4}>
                            <Grid item lg={3} sm={6} xl={3} xs={12}>
                                <InfoBox
                                    title="Nombre hospitalisations"
                                    value={data[data.length-1].hosp}
                                    differenceValue={formatter.format((data[data.length-1].hosp - data[data.length-8].hosp)/(data[data.length-8].hosp)*100)}
                                    caption="Sur 7 jours"
                                />
                            </Grid>
                            <Grid item lg={3} sm={6} xl={3} xs={12}>
                                <InfoBox
                                    title="Nombre réanimations"
                                    value={data[data.length-1].rea}
                                    differenceValue={formatter.format((data[data.length-1].rea - data[data.length-8].rea)/(data[data.length-8].rea)*100)}
                                    caption="Sur 7 jours"
                                />
                            </Grid>
                            <Grid item lg={3} sm={6} xl={3} xs={12}>
                                <InfoBox
                                    title="Nombre décès"
                                    value={data[data.length-1].dc_tot}
                                    differenceValue={formatter.format((data[data.length-1].dc_tot - data[data.length-8].dc_tot)/(data[data.length-8].dc_tot)*100)}
                                    caption="Sur 7 jours"
                                />
                            </Grid>
                            <Grid item lg={3} sm={6} xl={3} xs={12}>
                                <InfoBox
                                    title="Cas confirmés J-1"
                                    value={data[data.length-1].conf_j1}
                                    differenceValue={formatter.format((data[data.length-1].conf_j1 - data[data.length-8].conf_j1)/(data[data.length-8].conf_j1)*100)}
                                    caption="Sur 7 jours"
                                />
                            </Grid>
                            <Grid item lg={6} md={6} xl={6} xs={12}>
                                <Widget title="Facteur de propagation (R)">
                                    <LineChart data={data} x={"date"} y={"r"} ylim={1}/>
                                </Widget>
                            </Grid>
                            <Grid item lg={6} md={6} xl={6} xs={12}>
                                <Widget title="Taux de positivité des tests">
                                    <LineChart data={data} x={"date"} y={"tx_pos"}/>
                                </Widget>
                            </Grid>
                            <Grid item lg={6} md={6} xl={6} xs={12}>
                                <Widget title="Taux d'incidence">
                                    <LineChart data={data} x={"date"} y={"tx_incid"} ylim={50}/>
                                </Widget>
                            </Grid>
                        </Grid>
                        <div>
                        </div>
                    </>
                    }
                </div>

            </Container>
        </div>

    )
}

const useStyles = makeStyles((theme) =>
    createStyles({
        grid: {
            display: "flex",
        },
        title:{
            color: "white",
            marginRight: "50px"
        },
        logo: {
            maxWidth: 40,
            marginLeft: 20,
        },
    }),
);

export default Landing
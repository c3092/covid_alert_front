import * as React from 'react'
import { useCallback } from 'react'
import { Redirect, useLocation } from 'react-router-dom'

import { useKeycloak } from '@react-keycloak/web'
import Stack from "@mui/material/Stack";
import Button from "@mui/material/Button";

const Login = () => {

    const {keycloak} = useKeycloak()
    return (
        <>
            {!!keycloak?.authenticated ? (
                <Redirect to={"/home"}/>
            ):keycloak.login()}
        </>

    )
}

export default Login
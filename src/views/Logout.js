import * as React from 'react'
import { useCallback } from 'react'
import { Redirect, useLocation } from 'react-router-dom'

import { useKeycloak } from '@react-keycloak/web'
import Stack from "@mui/material/Stack";
import Button from "@mui/material/Button";

const Logout = () => {

    const {keycloak} = useKeycloak()
    return (
        <>
            {!!keycloak?.authenticated ? (
                keycloak.logout()
            ):<Redirect to={"/"}/>}
        </>

    )
}

export default Logout
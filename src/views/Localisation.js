import {useKeycloak} from "@react-keycloak/web";
import {useEffect, useState} from "react";
import axios from "axios";
import Stack from "@mui/material/Stack";
import Button from "@mui/material/Button";
import { usePosition } from '../hooks/usePosition';

import * as React from "react";
import AxiosInstance from "../utils/AxiosInstance";
import L from "leaflet";
import CustomTable from "../components/CustomTable";
import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet'

import 'leaflet/dist/leaflet.css'
import {Alert} from "@mui/material";
import {Container} from "@mui/material";

const Localisation  = () => {
    const { keycloak } = useKeycloak()
    const [kafkaUp,setKafkaUp] =useState(false);
    const watch = true;
    const {
        latitude,
        longitude,
        timestamp,
        error,
    } = usePosition(watch, { enableHighAccuracy: true });
    const [nonContactLocalisations, setNonContactLocalisations] = useState([])
    const [localisations,setLocalisations] = useState([]);
    const columns = [
        { field: 'location_id', headerName: 'ID', width: 70 },
        { field: 'latitude', headerName: 'latitude',type: 'number', width: 150 },
        { field: 'longitude', headerName: 'longitude', type: 'number',width: 150 },
        { field: 'location_date', headerName: 'Date', type: 'date',width: 300 },
    ]
    const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric',hour:'2-digit',minute:'2-digit' };

    const positiveMarker = L.icon({
        iconUrl: "https://icons.iconarchive.com/icons/emey87/trainee/48/Gps-icon.png",
        iconSize: [32, 37],
        iconAnchor: [16, 37]
    })
    const normalMarker = L.icon({
        iconUrl: "https://icons.iconarchive.com/icons/icons-land/vista-map-markers/128/Map-Marker-Marker-Outside-Azure-icon.png",
        iconSize: [32, 37],
        iconAnchor: [16, 37]
    })

    const [successOrError,setSuccessOrError] = useState(null)
    const [disabled,setDisabled] = useState(false)

    const fetchLocalisation = (data) => {
        AxiosInstance.get(`localisation/user/${data.id}`)
            .then(response => {
                let positive = response.data.positive
                let normal = response.data.normal
                positive.forEach(function (loc,index) {
                    loc.location_date = new Date(Date.parse(loc.location_date)).toLocaleDateString("fr-FR",options)
                });
                normal.forEach(function (loc,index) {
                    loc.location_date = new Date(Date.parse(loc.location_date)).toLocaleDateString("fr-FR",options)
                });


                for( let i=normal.length - 1; i>=0; i--){
                    for( let j=0; j<positive.length; j++){
                        if(normal[i] && (normal[i].latitude === positive[j].latitude) && (normal[i].longitude === positive[j].longitude) && (normal[i].user === positive[j].user) && (normal[i].date === positive[j].date)){
                            normal.splice(i, 1);
                        }
                    }
                }




                setLocalisations(positive)
                setNonContactLocalisations(normal)

            });

    }

    const sendLocation = () => {
        setDisabled(true)
        if(keycloak?.authenticated){
            keycloak.loadUserProfile().then((data)=>{

                const localisation = {
                    latitude: latitude,
                    longitude:longitude,
                    location_date:timestamp,
                    user: data.id
                }
                if(error === null && latitude !== null && longitude !== null) {
                    AxiosInstance.post(`localisation_kafka`, localisation)
                        .then(() => {
                            setSuccessOrError(true)
                            fetchLocalisation(data)
                        }).catch(()=>setSuccessOrError(false)
                    );
                }

            })
        }
        setDisabled(false)
    }

    useEffect( () => {
        if(keycloak?.authenticated) {
            keycloak.loadUserProfile().then((data) => {
                fetchLocalisation(data)
                console.log("token id : " + data.id)
                AxiosInstance.get(`localisation_kafka/ping`)
                    .then(response => {
                        setKafkaUp(response.data)
                    })
            })
        }
    },[])

    return (
        <div>


            {!!keycloak?.authenticated && (
                <div>
                    <Stack spacing={2} direction="row">
                        <Button variant="contained" disabled={disabled || !kafkaUp} onClick={sendLocation}>
                            Envoyer ma localisation
                        </Button>
                        {successOrError === true &&
                        <Alert onClose={() => {setSuccessOrError(null)}}>Localisation envoyée !</Alert>
                        }
                        {successOrError === false &&
                        <Alert onClose={() => {setSuccessOrError(null)}} severity="error">Impossible d'envoyer sa localisation</Alert>
                        }
                    </Stack>
                </div>
            )}

            { latitude !== 0 && longitude !== 0 &&
                <Container maxWidth={"lg"}>
                <MapContainer center={[latitude, longitude]} zoom={13} scrollWheelZoom={false} style={{height: "1000px"}}>
                    <TileLayer
                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    />
                    {nonContactLocalisations.map((l) => {
                        return (
                            <Marker icon={normalMarker} position={[l.latitude, l.longitude]}/>
                        )
                    })}
                    {localisations.map((l) => {
                        return (
                            <Marker icon={positiveMarker} position={[l.latitude, l.longitude]}/>
                        )
                    })}

                </MapContainer>
                </Container>
            }

        </div>


    )
}

export default Localisation
import {useEffect} from 'react'

function useDarkMode() {
    useEffect(() => {
        document.body.style.backgroundColor = '#0a1929'

        return () => {
            document.body.style.backgroundColor = '#fff'
        }
    })
}

export default useDarkMode
import axios from "axios";


const AxiosInstance = axios.create({
    baseURL: "https://covid-alert-polytech-gateway.cluster-ig5.igpolytech.fr/"
})

export default AxiosInstance;
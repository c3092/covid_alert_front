import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import {
    createTheme,
    Fab,
    Grid,
    IconButton, ThemeProvider,
} from "@mui/material";
import {makeStyles, createStyles, styled} from "@mui/styles";
import ShareIcon from '@mui/icons-material/Share';
import {CardActionArea, Chip, Stack} from '@mui/material';
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import fireLogo from "../assets/fire.png";
import Button from "@mui/material/Button";

const BasicCard = ({index,title,link,date,source,description}) => {

    const classes = useStyles();


    return (

        <>
            <Card>
                <CardActionArea>
                    <CardContent className={classes.cardContent}>
                        <Stack spacing={2} alignItems={"center"}>
                            <Chip sx={{ bgcolor: '#00DAA1' }} icon={<AccessTimeIcon/>}  label={<Typography gutterBottom variant="subtitle1" className={classes.dateLabel}>{date}</Typography>}  className={classes.chip} />
                            <Typography gutterBottom sx={{ wordWrap: "break-word" }} className={classes.title} variant="h5" component="h2">
                                <img src={fireLogo} alt="logo" className={classes.logo} />
                                {title}
                            </Typography>
                        </Stack>
                    </CardContent>
                </CardActionArea>
                <CardActions className={classes.cardAction} >

                    <ThemeProvider theme={customTheme}>
                        <Button  variant={"contained"} startIcon={<ShareIcon/>} onClick={() => {window.open(link, '_blank')}} >
                                Visiter le site
                        </Button>
                    </ThemeProvider>
                </CardActions>
            </Card>
        </>

    );
}

const customTheme = createTheme({
    palette: {
        primary: {
            main: "#41B3A3",
            secondary: "#84cdca",
            contrastText: "#ffffff"
        }
    },
    components: {
        MuiButton: {
            styleOverrides: {
                root: {
                    '&:hover': {
                        backgroundColor: "#84cdca",
                    },
                },
            },
        },
    },
});
const useStyles = makeStyles((theme) =>
    createStyles({


        chip:{
            '&:hover': {
                backgroundColor: "#41B3A3",
            },
        },


        cardContent: {
            //height: "100%",
            background: "#41B3A3",
            flexGrow: 1,


        },

        cardAction: {
            background: "#84cdca",
            justifyContent: 'center'
        },

        dateLabel: {
            display: 'flex',
            alignItems: 'center',
            flexWrap: 'wrap',
            color: "#f0e0e0",
            gap: "5px",

        },

        logo: {
            maxWidth: 40,
            marginRight: 10,
        },
        title:{
            color: "white",
            marginRight: "50px"
        },

    })
)


export default BasicCard
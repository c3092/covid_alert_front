import React from 'react';
import { Card, CardHeader, Divider, CardContent, IconButton } from '@mui/material';
import {createStyles, makeStyles} from "@mui/styles";


export default function Widget(props) {
    const classes = useStyles();
    const { children, title } = props;
    return (
        <Card elevation={0}>
            <CardHeader
                title={title}
                className={classes.cardHeader}
            />

            <CardContent className={classes.cardContent}>{children}</CardContent>
            <Divider light />
        </Card>
    );
}


const useStyles = makeStyles((theme) =>
    createStyles({



        cardContent: {
            //height: "100%",
            flexGrow: 1,


        },

        cardHeader: {
            background: "#84cdca",
            justifyContent: 'center'
        },


    })
)

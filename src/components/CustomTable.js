import * as React from 'react';
import { DataGrid } from '@mui/x-data-grid';


export default function CustomTable(props) {


    const data = props.data
    const columns = props.columns

    return (
        <div style={{ height: 400, width: '100%' }} >
            <DataGrid
                rows= {data}
                getRowId={(r) => r.location_id}
                pageSize={5}
                rowsPerPageOptions={[5]}
                columns={columns}
            />
        </div>
    );
}
import React from 'react';
import Carousel from 'react-material-ui-carousel'
import { Paper, Button } from '@mui/material'
import BasicCard from "./BasicCard";
import NavigateNextIcon from '@mui/icons-material/NavigateNext';
import newsLogo from "../assets/news2.png";
import {createStyles, makeStyles} from "@mui/styles";

const CustomCarousel = ({data}) => {

    const classes = useStyles();

    return (
        <Carousel
            NextIcon={<NavigateNextIcon/>}
            IndicatorIcon={<img src={newsLogo} className={classes.logo} alt="indicator"/>}
            activeIndicatorIconButtonProps={{
                style: {
                    backgroundColor: 'lightseagreen' // 2
                }
            }}
            fullHeightHover={false}     // We want the nav buttons wrapper to only be as big as the button element is
            navButtonsProps={{          // Change the colors and radius of the actual buttons. THIS STYLES BOTH BUTTONS
                style: {
                    backgroundColor: 'cornflowerblue',
                }
            }}

            indicatorContainerProps={{
                style: {
                    marginTop: '52px',
                }

            }}
        interval={7000}
        stopAutoPlayOnHover={false}
        navButtonsAlwaysVisible={true}>

            {
                data.map( (n, i) => <BasicCard index={n.key} title={n.title} date={n.date} description={n.description} link={n.link} source={n.source} /> )
            }
        </Carousel>
    )
}

function Item(props) {
    return (
        <Paper>
            <h2>{props.item.title}</h2>
            <p>{props.item.date}</p>

            <Button className="CheckButton">
                Check it out!
            </Button>
        </Paper>
    )
}

const useStyles = makeStyles((theme) =>
    createStyles({

        logo: {
            maxWidth: 40,
        },

    })
)

export default CustomCarousel
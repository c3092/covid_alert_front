import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import covidLogo from'../assets/anti-virus.png';
import {useHistory} from "react-router-dom";
import {useKeycloak} from "@react-keycloak/web";
import ExitToAppIcon from '@mui/icons-material/ExitToApp';
import {makeStyles} from "@mui/styles";
import {AccountCircle} from "@mui/icons-material";
import Stack from "@mui/material/Stack";

export default function ButtonAppBar() {
    const classes = useStyles()
    const { keycloak } = useKeycloak()
    const history = useHistory();
    const routeChange = (path) =>{
        history.push(path);
    }
    return (
        <Box sx={{ flexGrow: 1 }}>
            <AppBar position="static" style={{ background: 'transparent', boxShadow: 'none',marginBottom:"20px"}}>
                <Toolbar>

                    <img src={covidLogo} alt="logo" className={classes.logo} />
                    <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                        Covid Alert
                    </Typography>
                    {!keycloak?.authenticated ? (
                        <>
                            <Box sx={{display: {xs: 'none', md: 'flex'}}}>
                                <Stack direction="row" spacing={2}>
                                <Button  variant={"outlined"} size={"small"} onClick={ () => {
                                    routeChange("/login")
                                }}>Se connecter</Button>
                                <Button variant={"outlined"} size={"small"} onClick={ () => {
                                    routeChange("/register")
                                }}>S'inscrire</Button>
                                </Stack>
                            </Box>
                        </>
                    ):(
                        <Box sx={{display: { md: 'flex'}}}>
                            <IconButton
                                size="large"
                                edge="end"
                                aria-label="Account"
                                aria-haspopup="true"
                                color="inherit"
                                onClick={() => {
                                    routeChange("/home")
                                }}
                            >
                                <AccountCircle fontSize={"large"} />
                            </IconButton>
                            <IconButton
                                size="large"
                                aria-label="logout"
                                color="inherit"
                                onClick={() => {
                                    routeChange("/logout")
                                }}
                            >

                                <ExitToAppIcon fontSize={"large"}/>

                            </IconButton>
                        </Box>
                        )}
                </Toolbar>
            </AppBar>
        </Box>
    );
}

const useStyles = makeStyles({
    logo: {
        maxWidth: 40,
        marginRight: 20,
    },
    appbar: {
        background: 'transparent',
        boxShadow: 'none',
        marginBottom:"20px"
    }
});
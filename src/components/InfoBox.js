import React from 'react';
import clsx from 'clsx';
import {Card, CardContent, Grid, Typography, CardHeader, IconButton} from '@mui/material';
import MoreVertIcon from '@mui/icons-material/MoreVert'
import { createStyles, makeStyles } from '@mui/styles';

const InfoBox = props => {
    const { title, value, differenceValue, caption } = props;
    const negativeDifference = differenceValue && differenceValue.toString().charAt(0) === '-';
    const classes = useStyles({ negativeDifference, ...props });

    return (
        <Card sx={{
            backgroundColor: "#41b3a3",
            color: "white"
        }} className={clsx(classes.root)} elevation={0}>
            <CardHeader
                action={
                    <IconButton aria-label="settings">
                        <MoreVertIcon />
                    </IconButton>
                }
                title={title}
            />
            <CardContent>
                <Grid container justify="space-between">
                    <Grid item>
                        <Typography variant="h4">{value}</Typography>
                    </Grid>
                </Grid>
                <div className={classes.difference}>
                    <Typography className={classes.differenceValue} variant="body2" sx={{marginRight:"10px"}}>
                        {differenceValue}%
                    </Typography>
                    <Typography  variant="caption">
                        {caption}
                    </Typography>
                </div>
            </CardContent>
        </Card>
    );
};

const useStyles = makeStyles( {
    root: {
        height: '100%'
    },
    difference: {
        display: 'flex',
        alignItems: 'center'
    },
    differenceValue: {
        color: props =>
            props.negativeDifference ? "green" : "#D60000",
    }
});

export default InfoBox;
import React, {PureComponent, useEffect} from 'react';
import { AreaChart, Area, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer,ReferenceLine } from 'recharts';




const LineChart = ({data,x,y,ylim}) =>  {




        return (
            <ResponsiveContainer width="100%" minWidth={500} height={350}>
                <AreaChart
                    width={500}
                    height={400}
                    data={data}
                    margin={{
                        top: 10,
                        right: 30,
                        left: 0,
                        bottom: 0,
                    }}
                >
                    <defs>
                        <linearGradient id="colorUv" x1="0" y1="0" x2="0" y2="1">
                            <stop offset="5%" stopColor="#8884d8" stopOpacity={0.8}/>
                            <stop offset="95%" stopColor="#8884d8" stopOpacity={0}/>
                        </linearGradient>
                    </defs>

                    <XAxis dataKey={x} />
                    <YAxis/>
                    <ReferenceLine y={ylim}  stroke="red" />
                    <Tooltip />
                    <Area type="monotone" dataKey={y}  stroke="#8884d8" fillOpacity={1} fill="url(#colorUv)"  />
                </AreaChart>
            </ResponsiveContainer>
        );

}

export default LineChart

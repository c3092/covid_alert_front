import * as React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter as Router } from 'react-router-dom'

import { ReactKeycloakProvider } from '@react-keycloak/web'

import keycloak from './keycloak'
import { AppRouter } from './routes'

import './index.css'
import { Container } from '@mui/material';
import { StylesProvider } from '@mui/styles';

import useDarkMode from "./hooks/useDarkMode";
import NavBar from "./components/NavBar";
import AxiosInstance from "./utils/AxiosInstance";


const App = () => {
    useDarkMode()

    const eventLogger = (event, error) => {
    }

    AxiosInstance.interceptors.request.use(
        async config => {
            config.headers.Authorization = 'Bearer ' + keycloak.token
            return config;
        },
        error => {
            Promise.reject(error)
        }
    )
    const tokenLogger = (tokens) => {
    }

    return (
        <ReactKeycloakProvider
            authClient={keycloak}
            onEvent={eventLogger}
            onTokens={tokenLogger}
        >
            <StylesProvider injectFirst>
                <Router>
                    <NavBar/>
                    <Container maxWidth="xl">
                        <AppRouter />
                    </Container>
                </Router>
            </StylesProvider>
        </ReactKeycloakProvider>
    )
}

export default App
import * as React from 'react'
import {Redirect, Route, Switch } from 'react-router-dom'

import { useKeycloak } from '@react-keycloak/web'

import HomePage from '../views/Home'
import LoginPage from '../views/Login'
import LandingPage from '../views/Landing'
import LogoutPage from '../views/Logout'
import RegisterPage from '../views/Register'
import PrivateRoute from './utils'
import Localisation from "../views/Localisation";
import Account from "../views/Account";
import Document from "../views/Document"

export const AppRouter = () => {
    const { initialized } = useKeycloak()

    if (!initialized) {
        return <div>Loading...</div>
    }

    return (
            <Switch>
                <PrivateRoute roles={['app-user']} path="/home" component={HomePage} />
                <PrivateRoute roles={['app-user']} path="/localisation" component={Localisation} />
                <PrivateRoute roles={['app-user']} path="/account" component={Account} />
                <PrivateRoute roles={['app-user']} path="/document" component={Document} />
                <Route path="/login" component={LoginPage} />
                <Route path="/logout" component={LogoutPage} />
                <Route path="/register" component={RegisterPage} />
                <Route exact path="/" component={LandingPage} />
                <Route path="*">
                    <Redirect to="/" />
                </Route>
            </Switch>
    )
}